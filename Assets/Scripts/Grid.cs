﻿using System;

public class Grid<TObject>
{
    public int Width { get; }
    public int Height { get; }
    public int CellSize { get; }
    public TObject[,] GridObjects { get; }

    public Grid(int width, int height, int cellSize, Func<int, int, TObject> createObject)
    {
        Width = width;
        Height = height;
        CellSize = cellSize;
        GridObjects = new TObject[Width, Height];

        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Height; j++)
            {
                GridObjects[i, j] = createObject(i, j);
            }
        }
    }

    public TObject GetGridObjectAt(int x, int y)
    {
        return GridObjects[x, y];
    }
}