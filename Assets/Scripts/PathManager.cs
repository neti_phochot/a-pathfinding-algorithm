using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{
    [SerializeField] private int gridWidth = 10;
    [SerializeField] private int gridHeight = 10;
    [SerializeField] private int cellSize = 10;

    private Pathfinding _pathfinding;
    private Camera _camera;

    private Block _startBlock;
    private Block _endBlock;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Start()
    {
        Grid<Node> grid = new Grid<Node>(gridWidth, gridHeight, cellSize, CreateNode);
        _pathfinding = new Pathfinding(grid);
    }

    private Node CreateNode(int x, int y)
    {
        return new Node(x, y);
    }

    private void FindPath(Block startBlock, Block endBlock)
    {
        if (!startBlock || !endBlock) return;

        List<Node> paths =
            _pathfinding.FindPath(startBlock.Node.x, startBlock.Node.y, endBlock.Node.x, endBlock.Node.y);

        if (paths == null)
        {
            Debug.LogWarning("No path Found!");
            return;
        }

        for (int i = 0; i < paths.Count; i++)
        {
            Node node = paths[i];
            Debug.Log($"[<color=green>{i + 1}</color>] <color=red>X:{node.x}</color> | <color=red>Y:{node.y}</color>");
        }

        PathFindingVisual.Instance.NextSnapshot();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PathFindingVisual.Instance.IsPlaying = !PathFindingVisual.Instance.IsPlaying;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            PathFindingVisual.Instance.IsPlaying = false;
            PathFindingVisual.Instance.NextSnapshot();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            PathFindingVisual.Instance.IsPlaying = false;
            PathFindingVisual.Instance.PreviousSnapshot();
        }

        if (Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            Block block = GetBlockAtMousePosition();
            if (!block || !block.Node.walkable || block.Node.isEndNode) return;
            if (_startBlock)
            {
                _startBlock.Node.isStartNode = false;
                _startBlock.SetStatus(Block.NodeStatus.WALKABLE);
            }

            block.SetStatus(Block.NodeStatus.START_NODE);
            _startBlock = block;
            _startBlock.Node.isStartNode = true;
            FindPath(_startBlock, _endBlock);
        }

        if (Input.GetMouseButtonDown(1))
        {
            Block block = GetBlockAtMousePosition();
            if (!block || !block.Node.walkable || block.Node.isStartNode) return;
            if (_endBlock)
            {
                _endBlock.Node.isEndNode = false;
                _endBlock.SetStatus(Block.NodeStatus.WALKABLE);
            }

            block.SetStatus(Block.NodeStatus.END_NODE);
            _endBlock = block;
            _endBlock.Node.isEndNode = true;
            FindPath(_startBlock, _endBlock);
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            Block block = GetBlockAtMousePosition();
            if (!block || block.Node.isStartNode || block.Node.isEndNode) return;
            if (Input.GetKey(KeyCode.LeftShift) && block.Node.walkable)
            {
                block.Node.walkable = false;
                block.SetStatus(Block.NodeStatus.NOT_WALKABLE);
                FindPath(_startBlock, _endBlock);
                return;
            }

            if (Input.GetKey(KeyCode.LeftControl) && !block.Node.walkable)
            {
                block.Node.walkable = true;
                block.SetStatus(Block.NodeStatus.WALKABLE);
                FindPath(_startBlock, _endBlock);
                return;
            }
        }
    }

    private Block GetBlockAtMousePosition()
    {
        var ray = _camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit))
        {
            if (hit.transform.TryGetComponent(out Block block))
            {
                return block;
            }
        }

        return null;
    }
}