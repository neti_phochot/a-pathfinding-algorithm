public class Node
{
    public int x { get; }
    public int y { get; }

    public int gCost;
    public int hCost;
    public int fCost;
    public bool walkable;
    public Node parentNode;

    public bool isStartNode;
    public bool isEndNode;

    public Node(int x, int y)
    {
        this.x = x;
        this.y = y;
        walkable = true;
    }

    public void CalculateFCost()
    {
        fCost = gCost + hCost;
    }

    public Node CloneNode()
    {
        Node node = new Node(x, y)
        {
            gCost = gCost,
            hCost = hCost,
            fCost = fCost,
            walkable = walkable,
            isStartNode = isStartNode,
            isEndNode = isEndNode,
            parentNode = parentNode
        };
        return node;
    }
}