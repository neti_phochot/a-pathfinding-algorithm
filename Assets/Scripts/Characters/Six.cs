using UnityEngine;

public class Six : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public void SetDrying(bool isDrying)
    {
        animator.SetBool("drying", isDrying);
    }
}