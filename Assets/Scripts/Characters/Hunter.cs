﻿using UnityEngine;

namespace Characters
{
    public class Hunter : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        public void SetAttack(bool attack)
        {
            animator.SetBool("attack", attack);
        }

        public void SetSpeed(float value)
        {
            animator.speed = value;
        }
    }
}