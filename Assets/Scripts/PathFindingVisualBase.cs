﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;

public abstract class PathFindingVisualBase : MonoSingleton<PathFindingVisualBase>
{
    public float autoPlayDelay = 0.5f;

    protected List<SnapshotAction> SnapshotActions;
    private int _currentSnapshotIndex;

    public override void Awake()
    {
        base.Awake();
        SnapshotActions = new List<SnapshotAction>();
        ClearSnapshot();
    }

    public bool IsPlaying { get; set; }
    private float _autoPlayTimer;

    private void Update()
    {
        AutoPlay();
    }

    private void AutoPlay()
    {
        if (!IsPlaying || SnapshotActions.Count == 0) return;
        if (IsFinalSnapshot)
        {
            NextSnapshot();
            //IsPlaying = false;
        }

        if (_autoPlayTimer > 0f)
        {
            _autoPlayTimer -= Time.deltaTime;
            return;
        }

        _autoPlayTimer = autoPlayDelay;
        NextSnapshot();
    }

    public bool IsFinalSnapshot => _currentSnapshotIndex == SnapshotActions.Count - 1;

    public abstract void CreatePathFinding(Pathfinding pathfinding);

    public abstract void TakeSnapshot(Grid<Node> grid, Node currentNode, IEnumerable<Node> openList,
        IEnumerable<Node> closeList);

    public abstract void TakeSnapShotFinalPath(Grid<Node> grid, List<Node> paths);

    public void ClearSnapshot()
    {
        _currentSnapshotIndex = -1;
        SnapshotActions = new List<SnapshotAction>();
    }

    public void NextSnapshot()
    {
        if (++_currentSnapshotIndex >= SnapshotActions.Count)
        {
            _currentSnapshotIndex = 0;
        }

        SnapshotAction snapshotAction = SnapshotActions[_currentSnapshotIndex];
        snapshotAction.TriggerAction();
    }

    public void PreviousSnapshot()
    {
        if (--_currentSnapshotIndex < 0)
        {
            _currentSnapshotIndex = SnapshotActions.Count - 1;
        }

        SnapshotAction snapshotAction = SnapshotActions[_currentSnapshotIndex];
        snapshotAction.TriggerAction();
    }

    protected class SnapshotAction
    {
        private Action _action;

        public SnapshotAction()
        {
            _action = () => { };
        }

        public void AddAction(Action action)
        {
            _action += action;
        }

        public void TriggerAction()
        {
            _action();
        }
    }
}