﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Button settingButton;
        [SerializeField] private Button characterButton;
        [SerializeField] private TextMeshProUGUI characterStatusText;
        [SerializeField] private LittleNightmare littleNightmare;

        private void Awake()
        {
            settingButton.onClick.AddListener(() =>
            {
                if (canvasGroup.alpha > 0.5f)
                {
                    Hide();
                }
                else
                {
                    Show();
                }
            });

            characterButton.onClick.AddListener(() =>
            {
                littleNightmare.ShowCharacter(!littleNightmare.IsCharacterVisible);
                characterStatusText.text = littleNightmare.IsCharacterVisible ? "Hide Characters" : "Show Characters";
            });
        }

        private void Show()
        {
            canvasGroup.alpha = 1f;
        }

        private void Hide()
        {
            canvasGroup.alpha = 0f;
        }
    }
}