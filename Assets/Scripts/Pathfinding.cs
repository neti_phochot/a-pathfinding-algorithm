﻿using System.Collections.Generic;
using UnityEngine;

public class Pathfinding
{
    private const int CrossCost = 10;
    private const int DiagonalCost = 14;
    public Grid<Node> Grid { get; }

    private List<Node> _openListNode;
    private List<Node> _closeListNode;

    public Pathfinding(Grid<Node> grid)
    {
        Grid = grid;
        for (int i = 0; i < Grid.Width; i++)
        {
            for (int j = 0; j < Grid.Height; j++)
            {
                Grid.GetGridObjectAt(i, j);
            }
        }

        PathFindingVisualBase.Instance.CreatePathFinding(this);
    }

    public List<Node> FindPath(int startX, int startY, int endX, int endY)
    {
        PathFindingVisualBase.Instance.ClearSnapshot();

        foreach (var node in Grid.GridObjects)
        {
            node.gCost = int.MaxValue;
            node.hCost = 0;
            node.fCost = 0;
            node.parentNode = null;
            node.CalculateFCost();

            node.isStartNode = false;
            node.isEndNode = false;
        }

        Node startNode = Grid.GetGridObjectAt(startX, startY);
        startNode.isStartNode = true;

        Node endNode = Grid.GetGridObjectAt(endX, endY);
        endNode.isEndNode = true;

        _openListNode = new List<Node> {startNode};
        _closeListNode = new List<Node>();

        startNode.gCost = 0;
        startNode.CalculateFCost();

        while (_openListNode.Count > 0)
        {
            Node currentNode = FindLowestFValueNode();

            if (currentNode == endNode)
            {
                List<Node> foundPaths = CalculatePath(endNode);
                //PathFindingVisualBase.Instance.TakeSnapshot(Grid, currentNode, _openListNode, _closeListNode);
                PathFindingVisualBase.Instance.TakeSnapShotFinalPath(Grid, foundPaths);
                return foundPaths;
            }

            _openListNode.Remove(currentNode);
            _closeListNode.Add(currentNode);

            foreach (var neighbourNode in FindNeighbourNodes(currentNode))
            {
                if (_closeListNode.Contains(neighbourNode)) continue;
                if (!neighbourNode.walkable)
                {
                    _closeListNode.Add(neighbourNode);
                    continue;
                }

                int tentativeGCost = currentNode.gCost + CalculateHeuristicCost(currentNode, neighbourNode);
                if (tentativeGCost < neighbourNode.gCost)
                {
                    neighbourNode.gCost = tentativeGCost;
                    neighbourNode.hCost = CalculateHeuristicCost(neighbourNode, endNode);
                    neighbourNode.CalculateFCost();
                    neighbourNode.parentNode = currentNode;
                }

                if (!_openListNode.Contains(neighbourNode))
                {
                    _openListNode.Add(neighbourNode);
                }
            }

            PathFindingVisualBase.Instance.TakeSnapshot(Grid, currentNode, _openListNode, _closeListNode);
        }

        return null;
    }

    private Node FindLowestFValueNode()
    {
        Node lowestFValueNode = _openListNode[0];
        for (int i = 0; i < _openListNode.Count; i++)
        {
            var node = _openListNode[i];
            if (node.fCost <= lowestFValueNode.fCost)
            {
                if (node.hCost > lowestFValueNode.hCost) continue;
                lowestFValueNode = node;
            }
        }

        return lowestFValueNode;
    }

    private List<Node> CalculatePath(Node endNode)
    {
        List<Node> pathNodes = new List<Node> {endNode};
        Node currentNode = endNode;
        while (currentNode.parentNode != null)
        {
            pathNodes.Add(currentNode.parentNode);
            currentNode = currentNode.parentNode;
        }

        pathNodes.Reverse();
        return pathNodes;
    }

    private List<Node> FindNeighbourNodes(Node currentNode)
    {
        List<Node> neighbourNodes = new List<Node>();

        if (currentNode.x - 1 >= 0)
        {
            // Left
            neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x - 1, currentNode.y));
            // Left Down
            if (currentNode.y - 1 >= 0) neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x - 1, currentNode.y - 1));
            // Left Up
            if (currentNode.y + 1 < Grid.Width)
                neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x - 1, currentNode.y + 1));
        }

        if (currentNode.x + 1 < Grid.Width)
        {
            // Right
            neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x + 1, currentNode.y));
            // Right Down
            if (currentNode.y - 1 >= 0) neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x + 1, currentNode.y - 1));
            // Right Up
            if (currentNode.y + 1 < Grid.Height)
                neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x + 1, currentNode.y + 1));
        }

        // Down
        if (currentNode.y - 1 >= 0) neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x, currentNode.y - 1));
        // Up
        if (currentNode.y + 1 < Grid.Height)
            neighbourNodes.Add(Grid.GetGridObjectAt(currentNode.x, currentNode.y + 1));

        return neighbourNodes;
    }

    private int CalculateHeuristicCost(Node a, Node b)
    {
        //DIAGONAL DISTANCE
        int xDistance = Mathf.Abs(a.x - b.x);
        int yDistance = Mathf.Abs(a.y - b.y);
        int remaining = Mathf.Abs(xDistance - yDistance);
        return DiagonalCost * Mathf.Min(xDistance, yDistance) + CrossCost * remaining;
    }
}