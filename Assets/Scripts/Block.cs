﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour
{
    [SerializeField] private Transform arrowTransform;
    [SerializeField] private TextMeshProUGUI gCostText;
    [SerializeField] private TextMeshProUGUI hCostText;
    [SerializeField] private TextMeshProUGUI fCostText;

    [SerializeField] private Image backgroundImage;
    [SerializeField] private Color walkableColor = Color.white;
    [SerializeField] private Color notWalkableColor = Color.black;
    [SerializeField] private Color parentNodeColor = Color.blue;
    [SerializeField] private Color openNodeColor = Color.cyan;
    [SerializeField] private Color closeNodeColor = Color.magenta;
    [SerializeField] private Color startNodeColor = Color.green;
    [SerializeField] private Color endNodeColor = Color.red;
    [SerializeField] private Color pathNodeColor = Color.yellow;

    public Node Node { get; private set; }

    public enum NodeStatus
    {
        WALKABLE,
        NOT_WALKABLE,
        START_NODE,
        END_NODE,
        PARENT_NODE,
        OPEN_NODE,
        CLOSE_NODE,
        PATH_NODE,
    }


    public void Init(Node node)
    {
        Node = node;
        const int maxFCost = 1000;
        gCostText.text = Mathf.Abs(node.fCost) < maxFCost && node.hCost > 0 ? $"G={node.gCost}" : string.Empty;
        hCostText.text = Mathf.Abs(node.fCost) < maxFCost && node.hCost > 0 ? $"H={node.hCost}" : string.Empty;
        fCostText.text = Mathf.Abs(node.fCost) < maxFCost && node.hCost > 0 ? $"F={node.fCost}" : string.Empty;

        if (node.isStartNode)
        {
            hCostText.text = "START";
        }

        if (node.isEndNode)
        {
            hCostText.text = "GOAL";
        }
    }

    public void LookAt(Block block)
    {
        var lookDir = block.transform.position - transform.position;
        arrowTransform.rotation = Quaternion.LookRotation(lookDir);
    }

    private void ShowDirection()
    {
        arrowTransform.gameObject.SetActive(true);
    }

    private void HideDirection()
    {
        arrowTransform.gameObject.SetActive(false);
    }

    public void SetStatus(NodeStatus nodeStatus)
    {
        HideDirection();
        switch (nodeStatus)
        {
            case NodeStatus.WALKABLE:
                backgroundImage.color = walkableColor;
                break;
            case NodeStatus.NOT_WALKABLE:
                backgroundImage.color = notWalkableColor;
                break;
            case NodeStatus.START_NODE:
                backgroundImage.color = startNodeColor;
                break;
            case NodeStatus.END_NODE:
                backgroundImage.color = endNodeColor;
                break;
            case NodeStatus.PARENT_NODE:
                backgroundImage.color = parentNodeColor;
                break;
            case NodeStatus.OPEN_NODE:
                backgroundImage.color = openNodeColor;
                ShowDirection();
                break;
            case NodeStatus.CLOSE_NODE:
                backgroundImage.color = closeNodeColor;
                break;
            case NodeStatus.PATH_NODE:
                backgroundImage.color = pathNodeColor;
                break;
            default:
                backgroundImage.color = walkableColor;
                break;
        }
    }
}