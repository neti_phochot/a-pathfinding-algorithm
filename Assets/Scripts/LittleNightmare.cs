﻿using System.Collections.Generic;
using Characters;
using TMPro;
using UnityEngine;

public class LittleNightmare : MonoBehaviour
{
    public float moveSpeed;
    [SerializeField] private TextMeshProUGUI moveSpeedText;
    [Space] [SerializeField] private PathFindingVisual pathFindingVisual;
    [Header("CHARACTER")] [SerializeField] private Six six;
    [SerializeField] private Hunter hunter;
    [SerializeField] private float rotateSpeed = 10f;
    private Queue<Block> _pathQueues;

    public bool IsCharacterVisible => six.gameObject.activeSelf;

    private void Awake()
    {
        _pathQueues = new Queue<Block>();
        pathFindingVisual.OnFoundPath += OnFoundPath;
        UpdateMoveSpeedText(moveSpeed);
    }


    private void OnFoundPath(List<Block> paths)
    {
        _pathQueues = new Queue<Block>(paths);

        Block startBlock = paths[0];
        Block endBlock = paths[paths.Count - 1];

        six.transform.position = endBlock.transform.position;
        six.SetDrying(false);

        hunter.transform.position = startBlock.transform.position;
        hunter.SetAttack(false);
    }

    private void Update()
    {
        MoveEnemyToPlayer();
    }

    private void MoveEnemyToPlayer()
    {
        if (_pathQueues.Count == 0) return;
        Vector3 moveTo = _pathQueues.Peek().transform.position;
        if (Vector3.Distance(moveTo, hunter.transform.position) > 0.1f && _pathQueues.Count > 1)
        {
            MoveEnemy(moveTo);
            return;
        }

        if (_pathQueues.Count == 1)
        {
            hunter.SetAttack(true);
            Invoke("Drying", 0.5f);
        }

        _pathQueues.Dequeue();
    }


    private void MoveEnemy(Vector3 moveTo)
    {
        var hunterTransform = hunter.transform;
        var position = hunterTransform.position;
        Vector3 dirToTarget = (moveTo - position).normalized;
        position += hunterTransform.forward * (moveSpeed * Time.deltaTime);
        hunterTransform.position = position;
        hunter.transform.localRotation = Quaternion.Lerp(hunterTransform.localRotation,
            Quaternion.LookRotation(dirToTarget),
            Time.deltaTime * rotateSpeed);
    }

    private void Drying()
    {
        six.SetDrying(true);
    }

    public void OnMoveSpeedValueChanged(float value)
    {
        moveSpeed = value;
        UpdateMoveSpeedText(value);
        hunter.SetSpeed(value);
    }

    private void UpdateMoveSpeedText(float value)
    {
        moveSpeedText.text = $"Move Speed: {value:F1}";
    }

    public void ShowCharacter(bool show)
    {
        six.gameObject.SetActive(show);
        hunter.gameObject.SetActive(show);
    }
}