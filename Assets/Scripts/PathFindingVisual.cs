﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PathFindingVisual : PathFindingVisualBase
{
    public Action<List<Block>> OnFoundPath;

    [SerializeField] private TextMeshProUGUI updateNodeDelayText;
    [SerializeField] private Block blockPrefab;

    public Block[,] Blocks { get; private set; }

    public override void Awake()
    {
        base.Awake();
        UpdateNodeDelayText(autoPlayDelay);
    }

    public void OnUpdateNodeDelayChanged(float value)
    {
        autoPlayDelay = value;
        UpdateNodeDelayText(value);
    }

    private void UpdateNodeDelayText(float value)
    {
        updateNodeDelayText.text = $"Update Delay: {value:F1}s";
    }

    public override void CreatePathFinding(Pathfinding pathfinding)
    {
        Grid<Node> grid = pathfinding.Grid;
        Blocks = new Block[grid.Width, grid.Height];
        for (int i = 0; i < grid.Width; i++)
        {
            for (int j = 0; j < grid.Height; j++)
            {
                Block block = Instantiate(blockPrefab, transform);
                Blocks[i, j] = block;

                Node node = grid.GetGridObjectAt(i, j);
                block.Init(node);
                Transform blockTransform = block.transform;
                blockTransform.localScale = new Vector3(grid.CellSize, 1, grid.CellSize);
                blockTransform.position = new Vector3(node.x * grid.CellSize, 0, node.y * grid.CellSize);
            }
        }

        UpdateVisual(grid);
    }

    public override void TakeSnapshot(Grid<Node> grid, Node currentNode, IEnumerable<Node> openList,
        IEnumerable<Node> closeList)
    {
        SnapshotAction snapshotAction = new SnapshotAction();

        List<Node> newOpenList = new List<Node>(openList);
        List<Node> newCloseList = new List<Node>(closeList);
        Node[,] cloneGrid = CloneGrid(grid);

        snapshotAction.AddAction(() =>
        {
            UpdateVisual(cloneGrid);

            foreach (var openNode in newOpenList)
            {
                if (openNode.isStartNode || openNode.isEndNode || !openNode.walkable) continue;
                Block openBlock = Blocks[openNode.x, openNode.y];
                openBlock.SetStatus(Block.NodeStatus.OPEN_NODE);
            }

            foreach (var closeNode in newCloseList)
            {
                if (closeNode.isStartNode || closeNode.isEndNode || !closeNode.walkable) continue;
                Block closeBlock = Blocks[closeNode.x, closeNode.y];
                closeBlock.SetStatus(Block.NodeStatus.CLOSE_NODE);
            }

            Block currentBlock = Blocks[currentNode.x, currentNode.y];
            currentBlock.SetStatus(Block.NodeStatus.PARENT_NODE);
        });

        SnapshotActions.Add(snapshotAction);
    }

    public override void TakeSnapShotFinalPath(Grid<Node> grid, List<Node> paths)
    {
        SnapshotAction snapshotAction = new SnapshotAction();
        snapshotAction.AddAction(() =>
        {
            UpdateVisual(grid);

            List<Block> blocks = new List<Block>();
            foreach (var node in paths)
            {
                Block block = Blocks[node.x, node.y];
                blocks.Add(block);

                block.SetStatus(Block.NodeStatus.PATH_NODE);
                if (node.isStartNode)
                {
                    block.SetStatus(Block.NodeStatus.START_NODE);
                }

                if (node.isEndNode)
                {
                    block.SetStatus(Block.NodeStatus.END_NODE);
                }
            }

            OnFoundPath?.Invoke(blocks);
        });

        SnapshotActions.Insert(0, snapshotAction);
    }

    private void UpdateVisual(Grid<Node> grid)
    {
        for (int i = 0; i < grid.Width; i++)
        {
            for (int j = 0; j < grid.Height; j++)
            {
                Node node = grid.GetGridObjectAt(i, j);
                Block block = Blocks[i, j];
                block.Init(node);
                block.SetStatus(node.walkable ? Block.NodeStatus.WALKABLE : Block.NodeStatus.NOT_WALKABLE);

                Node parentNode = block.Node.parentNode;
                if (parentNode != null)
                {
                    Block parentBlock = Blocks[parentNode.x, parentNode.y];
                    block.LookAt(parentBlock);
                }

                if (node.isStartNode)
                {
                    block.SetStatus(Block.NodeStatus.START_NODE);
                }

                if (node.isEndNode)
                {
                    block.SetStatus(Block.NodeStatus.END_NODE);
                }
            }
        }
    }

    private void UpdateVisual(Node[,] nodes)
    {
        for (int i = 0; i < nodes.GetLength(0); i++)
        {
            for (int j = 0; j < nodes.GetLength(1); j++)
            {
                Node node = nodes[i, j];
                Block block = Blocks[i, j];
                block.Init(node);
                block.SetStatus(node.walkable ? Block.NodeStatus.WALKABLE : Block.NodeStatus.NOT_WALKABLE);

                Node parentNode = block.Node.parentNode;
                if (parentNode != null)
                {
                    Block parentBlock = Blocks[parentNode.x, parentNode.y];
                    block.LookAt(parentBlock);
                }

                if (node.isStartNode)
                {
                    block.SetStatus(Block.NodeStatus.START_NODE);
                }

                if (node.isEndNode)
                {
                    block.SetStatus(Block.NodeStatus.END_NODE);
                }
            }
        }
    }

    private Node[,] CloneGrid(Grid<Node> grid)
    {
        Node[,] cloneNodes = new Node[grid.Width, grid.Height];

        for (int i = 0; i < grid.Width; i++)
        {
            for (int j = 0; j < grid.Height; j++)
            {
                cloneNodes[i, j] = grid.GetGridObjectAt(i, j).CloneNode();
            }
        }

        return cloneNodes;
    }
}